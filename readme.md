# Descripcion
Manual para facilitar el entendimiento de la máquina virtual creada en este link https://www.youtube.com/watch?v=MF4qRSedmEs, sin embargo esta recibió algunas modificaciones.
La máquina virtual está alojada en el siguiente link : https://drive.google.com/drive/folders/1WZUBHsGS8rNJu9FD9J4lTF6eCensdQ7d?usp=sharing

# Credenciales
User : `cibersecfiis`

Password: `toor`

# Interfaz

`Nota: Está podrá ser personalizada al gusto del usuario, eso se verá en el apartado "PERSONALIZANDO"`

![alt text](https://gitlab.com/alfonso2297/parrot-security-cibersecfiis/-/raw/master/src/common/images/barra.png "Barra") 

### Detallando
![alt text](https://gitlab.com/alfonso2297/parrot-security-cibersecfiis/-/raw/master/src/common/images/escritorios.png "Escritorios") 
- Circulo lleno : Escritorio activo.
- Circulo con punto: Escritorio inactivo pero con programas abiertos.
- Circulo vacío: Escritorio inactivo y sin programas.

![alt text](https://gitlab.com/alfonso2297/parrot-security-cibersecfiis/-/raw/master/src/common/images/app-activa.png "Activa") 
- Nombre del programa activo.

![alt text](https://gitlab.com/alfonso2297/parrot-security-cibersecfiis/-/raw/master/src/common/images/caracteristicas.png "Caracteristicas") 
- Volumen actual (puede modificarse con la rueda del ratón)
- Logo de objetivo seguido de su IP (no viene incluido en el OVA, se mostrará el código en "PERSONALIZACIÓN")
- Logo de HackTheBox seguido del IP asignado.
- Logo de red seguido del IP de la red (eth0)
- Porcentaje de uso del sistema
- Hora

# Atajos predeterminados
`Nota: Para esta configuración se está utilizando la tecla Windows como la tecla para lanzar atajos`
## Gestion de sesión
#### Cambiar entre escritorios
` Windows + {Número del escritorio} `
#### Cerrar sesión
` Windows + Alt + Q `
#### Volver a cargar bspwn (La barra)
` Windows + Alt + R`

#### Bloquear pantalla 
` Windows + Control + Alt + X `

## Iniciar aplicaciones
#### Terminal
` Windows + Intro `

#### Chrome
` Windows + Shift + G `

#### Lanzador de apps
` Windows + D `

## Manejar ventanas
#### Cambiar entre ventanas
` Windows + Flecha direccional `

#### Cerrar programa 
` Windows + W `

#### Pantalla completa ( Modo Zen )
` Windows + F `

#### Pantalla completa
` Windows + M `


#### Ventana flotante (se necesitará un mouse)
` Windows + T `
` Windows + Shift + T `



#### Ir al ultimo escritorio usado
` Windows + Tab `

#### Para indicar donde crearás la siguiente aplicación (ya debe haber una abierta)
` Ctrl + Windows + Alt + {Flecha dirección} `
`Nota: Aquí se notará un región anaranjada en la cual se creará la siguiente ventana que se abra, esta puede ser una terminal o un programa cualquiera, todo usando atajos, Ejm: windows + Intro para la terminal`

#### Personalizar tamaño de la nueva ventana
`Nota: Con la ventana sombreada (el color anaranjado señalado en el atajo anterior), éste atajo permitirá elegir el tamaño`
` Ctrl + Windows + {Número 1 - 9} `

#### Cancelar selección (anaranjado)
` Ctrl + Windows + Space `

#### Mover ventanas ( estando en modo ventana flotante )
` Ctrl + Windows + {Flecha direccion}`

#### Modificar tamaño de ventana (modo flotante)
` Ctrl + Alt + {Flecha dirección} `
 

# Características de la terminal zsh
- Presione 2 veces "Esc" para ingresar la palabra "sudo" a su comando.
- Se tiene autocompletado, el cual se basa en el archivo zsh_history, es decir, que autocompleta solo comandos que hayan sido ingresados previamente.
- El comando cat permite ver el archivo con una numeración al lado izquierdo.
- El comando ls muestra iconos para cada tipo de archivo.
- Se indica, con colores, si el comando existe o no.
- El promt indica el tiempo que ha tomado cada operación realizada previamente y si fue exitosa o no.
- Ingresar `Windows + Ctrl + R ` para buscar entre comandos ingresados previamente.

# Personalizando

### Añadiendo atajos propios
A tener en cuenta: esta configuración usa SXHKD para los atajos, toda la configuración de estos se encuentra en su fichero de configuración.

Modificar el shkdrc:
```bash
nano /home/cibersecfiis/.config/sxhkd/sxhkdrc
```
Aqui se puede ver la siguiente estructura 
```bash
Tecla 1 + Tecla 2 + ....
    comando a lanzar
```

`Nota: si se agrega "gksudo" antes del comando a lanzar este correrá como root`

`Nota 2 : Aquí pueden modificar si algún atajo no es de su agrado`

### Modificar el gestor de ventanas
* A tener en cuenta: El programa que se usa como gestor de ventanas es bspwn, este es altamente personalizable.

- El archivo de configuración está en:
```bash
nano /home/cibersecfiis/.config/bspwn/bspwnrc
```
`Nota: este solo servirá para configurar la apariencia de las ventanas si se desea añadir características a la barra, se tratará en el siguiente punto.`


### Añadir características a la barra
* A tener en cuenta: El programa que se usa para la barra se llama polybar y esta permite añadir prácticamente cualquier cosa en la barra superior, trataremos sobre como añadir funciones si se desea, o eliminar las existentes, además de crear las nuestras propias.


- Para modificar las características de la barra ir a:
```bash
nano /home/cibersecfiis/.config/polybar/config
```
- El archivo de configuración se estructura de la siguiente manera:
1.- El apartado `[colors]` será para configurar los colores en la polybar además de algunas configuraciones adicionales (Se recomienda ir probando configuraciones hasta encontrar la deseada)
2.- En la siguiente tenemos `[bar/example]`, hay que tener en cuenta que la configuración de la barra que usaremos será esta, es decir, en esta se encuentran los modulos que estamos usando, cabe recalcar que la personalización de la barra radica en este apartado principalmente, la opción anterior de `[colors]` unicamente es para crear variables con valores de colores para ahora ser llamados en este apartado `[bar/example]`.
Podemos describir la tabla de la siguiente manera:
- Sección de configuración: esta es toda la primera parte, en esta se definen los colores de la polybar, el tamaño, la distancia a los bordes, etc.
- La sección que nos interesa para añadir características será a partir de ` modules-left / modules-center / modules-right`, ya que en cada uno indica que modulos tendremos en cada lado (izquierda, centro y derecha). Podemos notar que actualmente tenemos como modulo en la izquierda el bspwn y el i3, los cuales nos permiten ver los escritorios en forma de puntos. Seguido de eso, en el centro tenemos el xwindow el cual es el nombre de la ventana actual. Y finalmente, a la derecha, tenemos pulseaudio (para el volumen), vpn (para el IP que nos asignó cualquier vpn a la cual nos hayamos conectado), ethernet (para conocer mi IP actual), memory (el uso actual de memoria) y date (Fecha actual). Finalmente, debemos notar que aquí es donde colocaremos los módulos que deseemos añadir.
3.- Todo lo demás fuera de `[bar/example]` serán las configuraciones de cada módulo, ahí podrán ver que módulos pueden llamar en la polybar o añadir alguno que deseen crear.
4.- Ahora, para explicar como añadir un módulo que no existe previamente crearemos un módulo que nos permita mostrar en la barra el IP del objetivo actual que tenemos, para ello debemos considerar que lo que muchas veces haremos es mostrar el resultado de un comando en la barra, por ejemplo, el modulo ethernet lo que hace es lanzar el comando `ip a` y cortarlo hasta solo tener la IP.
a) Dado que no tenemos ningún comando predefinido para almacenar un valor que contenga la IP, primero crearemos un ejecutable que nos permita hacer eso. Nos vamos a la carpeta de los ejecutables y creamos el archivo target (el nombre depende del usuario).
```bash
nano /user/bin/target
```
El contenido de este código será:
```bash
if [ -z "$1" ]; then
    echo "No se ingresó nada"
else 
    echo "Se ingresó el parámetro $1"
    echo $1 > /home/cibersecfiis/.config/bin/target/target.txt
fi
```
Lo guardamos y le damos permiso de ejecución
```bash
sudo chmod +x target
```
`Nota: ahora cada vez que necesitemos fijar un "objetivo", bastará con colocar en el terminal "target 10.10.10.160", siendo el IP ultima el del objetivo a atacar.`


b) Ahora iremos a la carpeta donde alojaremos nuestros módulos para la polybar, la dirección recomendada es:
```bash
$ cd /home/cibersecfiis/.config/bin
$ mkdir target
$ cd target
nano target.sh
```
Dentro de ese archivo pondremos un texto con la siguiente estructura. `Importante: Los logos se obtienen como letras, la fuente que se está usando es Nerd Fonts, para obtener un icono solo deberá ir a la pagina de Nerd Fonts y buscar su icono y copiarlo como texto, al pegarlo en el archivo ya debería ver su icono, ya que la terminal soporta dicha fuente.`
```bash
#! /bin/sh
echo "%{F#color}logo %{F#color}$(comando_a_lanzar)%{u-}"
```
Entonces quedaría así (en logo debería ir el icono obtenido de Nerd Fonts, pero como Markdown no deja visualizarlo preferí no incluirlo)

```bash
#! /bin/sh
echo "%{F#ff0000}logo %{F#e2ee6a}$(cat /home/cibersecfiis/.config/bin/target/target.txt)%{u-}"
```

Finalmente tenemos que darle permisos de ejecución a este sh
```bash
chmod +x target.sh
```
c) Ahora iremos a la configuración de la polybar
```bash
nano ~/.config/polybar/config
```
Vamos al final de todo y colocamos el nuevo módulo, el apartado de módulos se comporta de la siguiente manera
```bash
[module/nombre_del_modulo]
type = custom/script
interval = 2 (frecuencia de ejecución)
exec = .sh_a_ejecutar
```
Entonces quedaría de la siguiente manera
```bash
[module/target]
type = custom/script
interval = 2
exec = ~/.config/bin/target/target.sh
```

d) Ahora deberíamos decirle a la polybar que lo agrege a donde deseemos, en mi caso deseo ponerlo ala derecha de la barra, entonces añado `target` a `modules-right` quedando así
```bash
modules-right = pulseaudio target vpn ethernet memory date
```
Y guardaríamos los cambios

f) Finalmente para ver los cambios bastará con reiniciar la polybar con `Windows + Alt + R ` 
Debería quedar así
![alt text](https://gitlab.com/alfonso2297/parrot-security-cibersecfiis/-/raw/master/src/common/images/barra.png "Barra") 

### Cambiar wallpaper
- Opción 1 (Cambiar el archivo del fondo):

Cambiar el nombre de tu fondo a wallpaper.png
Pegar y reemplazar tu nuevo fondo en /home/cibersecfiis/Pictures 

- Opción 2 (Cambiar de donde se obtiene el fondo):

Dirigirse al fichero donde está la ruta del wallpaper:
``` bash
nano /home/cibersecfiis/.config/bspwn/bspwnrc
```
Modificar la línea 6 con la ruta del nuevo fondo.

# Adicionales
- Para añadir audio, agregar dispositivo a la maquina virtual.
` Opciones de VM (Settings) -> Añadir (Add) -> Tarjeta de sonido (Sound Card) -> Finish -> Guardar (Save) `

- Cerrar sesión desde el terminal 
`$ kill -9 -1`

- Si el fondo se ha movido durante el cambio de resolución que hace una maquina virtual, bastará reiniciar el bspwn (Lanzar `Windows + Shift + R`)

- Se puede añadir la palabra "sudo" antes de su comando, presionando dos veces "Esc"
